//package com.iwconnect.labs.test;
//
//import com.iwconnect.labs.conf.FunctionConfiguration;
//import com.iwconnect.labs.student.domain.ItemMaster;
//import com.iwconnect.labs.student.service.ItemMasterService;
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.MockitoJUnitRunner;
//
//import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
//import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
//
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Optional;
//
//import static org.mockito.Mockito.*;
//
//@RunWith(MockitoJUnitRunner.class)
//public class FindByItemIdFunctionTest {
//
//    @InjectMocks
//    private FunctionConfiguration functionConfiguration;
//
//    @Mock
//    private ItemMasterService itemMasterService;
//
//    @Test
//    public void testFindItemById() {
//        APIGatewayProxyRequestEvent proxyRequestEvent = new APIGatewayProxyRequestEvent();
//        Map<String, String> queryParams = new HashMap<>();
//        queryParams.put("itemId", "123");
//        proxyRequestEvent.setQueryStringParameters(queryParams);
//
//        ItemMaster item = new ItemMaster();
//        item.setItemId(123L);
//        item.setSlNo(9765L);
//        item.setStatus("success");
//       /* item.setName("Test Item");
//        item.setDescription("This is a test item.");*/
//
//        Mockito.when(itemMasterService.getItemById(123L)).thenReturn(item);
//
//        APIGatewayProxyResponseEvent response = functionConfiguration.findItemById().apply(proxyRequestEvent);
//
//        Assert.assertEquals(Optional.of(200), Optional.of(response.getStatusCode()));
//        Assert.assertEquals(item.toString(), response.getBody());
//    }
//}
//
