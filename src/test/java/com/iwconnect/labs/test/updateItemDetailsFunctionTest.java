//package com.iwconnect.labs.test;
//
//
//import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.iwconnect.labs.conf.FunctionConfiguration;
//import com.iwconnect.labs.student.domain.ItemMaster;
//import com.iwconnect.labs.student.dto.ItemMasterUpdatePojo;
//import com.iwconnect.labs.student.service.ItemMasterService;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.MockitoJUnitRunner;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import static org.junit.Assert.assertEquals;
//import static org.mockito.Mockito.when;
//
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class updateItemDetailsFunctionTest {
//
//    private static Logger logger = LoggerFactory.getLogger(FunctionConfiguration.class);
//
//    @MockBean
//    private ItemMasterService itemMasterService;
//
//    @Autowired
//    private FunctionConfiguration updateItemDetailsFunction;
//
//    private ObjectMapper mapper = new ObjectMapper();
//
//    @Test
//    public void testUpdateItemDetails() throws Exception {
////        APIGatewayProxyRequestEvent requestEvent = new APIGatewayProxyRequestEvent();
////        // Set up request event properties as needed
//
//        ItemMasterUpdatePojo itemMasterUpdatePojo = new ItemMasterUpdatePojo();
//        // Set up itemMasterUpdatePojo properties as needed
//        itemMasterUpdatePojo.setCategory("AAA");
//        itemMasterUpdatePojo.setUpdatedDate("12222");
//        itemMasterUpdatePojo.setItemId(10l);
//        itemMasterUpdatePojo.setUpdatedBy("naveen");
//        itemMasterUpdatePojo.setStatus("success");
//        itemMasterUpdatePojo.setCreatedBy("BBB");
//        itemMasterUpdatePojo.setUpdatedBy("CCC");
//        itemMasterUpdatePojo.setFocusArea("eeeeee");
//        itemMasterUpdatePojo.setSlNo(1000l);
//
//
//        ItemMaster itemMaster = new ItemMaster();
//        // Set up itemMaster properties as needed
//        itemMaster.setCategory("AAA");
//        itemMaster.setUpdatedDate("12222");
//        itemMaster.setItemId(10l);
//        itemMaster.setUpdatedBy("naveen");
//        itemMaster.setStatus("success");
//        itemMaster.setCreatedBy("BBB");
//        itemMaster.setUpdatedBy("CCC");
//        itemMaster.setFocusArea("eee");
//        itemMaster.setSlNo(1000l);
//
//
//        // Mock itemMasterService.updateItemDetails() to return the mocked itemMaster
//        when(itemMasterService.updateItemDetails(itemMasterUpdatePojo)).thenReturn(itemMaster);
//
//        // Call the function and get the response event
//        APIGatewayProxyResponseEvent responseEvent =apply(itemMasterUpdatePojo);
//        // Assert that the response event status code is 200
//        assertEquals(200, responseEvent.getStatusCode().intValue());
//
//        // Assert that the response event body is the JSON representation of the itemMaster object
//        String expectedBody = mapper.writeValueAsString(itemMaster);
//        assertEquals(expectedBody, responseEvent.getBody());
//
//    }
//
//    public APIGatewayProxyResponseEvent apply(ItemMasterUpdatePojo itemMasterUpdatePojo) {
//        logger.info("Execute createResponseEvent method");
//        APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
//        ObjectMapper mapper = new ObjectMapper();
//        try {
//            responseEvent.setStatusCode(200);
//            responseEvent.setHeaders(createResultHeader());
//            responseEvent.setBody(mapper.writeValueAsString(itemMasterUpdatePojo));
//        } catch (Exception e) {
//            logger.error("Error executing createResponseEvent method", e);
//            return new APIGatewayProxyResponseEvent().withStatusCode(500);
//        }
//        return responseEvent;
//    }
//
//    private Map<String, String> createResultHeader() {
//        logger.info("Execute createResultHeader method");
//        Map<String, String> resultHeader = new HashMap<>();
//        resultHeader.put("Content-Type", "application/json");
//
//        return resultHeader;
//    }
//}
//
//
//
