package com.iwconnect.labs.student.repository;

import com.iwconnect.labs.student.domain.ItemMaster;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemMasterRepository extends CrudRepository<ItemMaster, String> {

}
