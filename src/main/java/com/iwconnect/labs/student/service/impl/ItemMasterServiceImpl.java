package com.iwconnect.labs.student.service.impl;

import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.iwconnect.labs.student.domain.ItemMaster;
import com.iwconnect.labs.student.domain.ResponseMaster;
import com.iwconnect.labs.student.dto.ItemMasterPojo;
import com.iwconnect.labs.student.dto.ItemMasterUpdatePojo;
import com.iwconnect.labs.student.repository.ItemMasterRepository;
import com.iwconnect.labs.student.repository.ResponseMasterRepository;
import com.iwconnect.labs.student.service.ItemMasterService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ItemMasterServiceImpl implements ItemMasterService {

    @Autowired
    ItemMasterRepository itemMasterRepository;

    @Autowired
    ResponseMasterRepository responseMasterRepository;

    /**
     * This method saves the item details in itemMaster table.
     *
     * @param itemMasterPojo
     */
    @Override
    public ItemMaster saveItemDetails(ItemMasterPojo itemMasterPojo) throws Exception {
        ItemMaster itemMaster = null;
        ResponseMaster response = null;
        if (Objects.nonNull(itemMasterPojo)) {
            itemMaster = ItemMaster.builder()
                    .slNo(itemMasterPojo.getSlNo())
                    .item(itemMasterPojo.getItem())
                    .category(itemMasterPojo.getCategory())
                    .focusArea(itemMasterPojo.getFocusArea())
                    .createdDate(itemMasterPojo.getCreatedDate())
                    .updatedDate(itemMasterPojo.getUpdatedDate())
                    .createdBy(itemMasterPojo.getCreatedBy())
                    .updatedBy(itemMasterPojo.getUpdatedBy())
                    .status(itemMasterPojo.getStatus())
                    .build();
            itemMasterRepository.save(itemMaster);

            List<ResponseMaster> responseMasterList = itemMasterPojo.getResponse();
            for (ResponseMaster responseMaster : responseMasterList) {
                response = ResponseMaster.builder()
                        .responseId(responseMaster.getResponseId())
                        .itemId(itemMaster)
                        .response(responseMaster.getResponse())
                        .rating(responseMaster.getRating())
                        .createdDate(responseMaster.getCreatedDate())
                        .updatedDate(responseMaster.getUpdatedDate())
                        .createdBy(responseMaster.getCreatedBy())
                        .updatedBy(responseMaster.getUpdatedBy())
                        .status(responseMaster.getStatus())
                        .build();
                responseMasterRepository.save(response);
            }

        }
        return itemMaster;

    }

    /**
     * This method retrieves an item by its ID from the itemMaster table.
     *
     * @param itemId
     * @return The ItemMaster object containing the item details.
     */
    @Override
    public ItemMaster getItemById(String itemId) {
        Optional<ItemMaster> item = itemMasterRepository.findById(itemId);
        if (item.isPresent() && item != null) {
            return item.get();
        } else {
            throw new ResourceNotFoundException("Item not found with ID = " + itemId);
        }
    }

    /**
     * This method updates the details of an existing item details in the itemMaster table.
     *
     * @param itemMasterUpdatePojo
     */
    @Override
    public ItemMaster updateItemDetails(ItemMasterUpdatePojo itemMasterUpdatePojo) throws Exception {
        Optional<ItemMaster> itemId = itemMasterRepository.findById(itemMasterUpdatePojo.getItemId());
        if (itemId.isPresent() && itemId != null) {
            ItemMaster itemMaster = itemId.get();
            itemMaster.setCategory(itemMasterUpdatePojo.getCategory());
            itemMaster.setUpdatedBy(itemMasterUpdatePojo.getUpdatedBy());
            itemMaster.setCreatedBy(itemMasterUpdatePojo.getCreatedBy());
            itemMaster.setItem(itemMasterUpdatePojo.getItem());
            itemMaster.setStatus(itemMasterUpdatePojo.getStatus());
            itemMaster.setSlNo(itemMasterUpdatePojo.getSlNo());
            itemMaster.setFocusArea(itemMasterUpdatePojo.getFocusArea());
            itemMaster.setUpdatedDate(itemMasterUpdatePojo.getUpdatedDate());
            itemMaster.setCreatedDate(itemMasterUpdatePojo.getCreatedDate());

            itemMasterRepository.save(itemMaster);

            List<ResponseMaster> responseMasterList = itemMasterUpdatePojo.getResponse();

            for (ResponseMaster responseMaster : responseMasterList) {
                ResponseMaster response = responseMasterRepository.findById(responseMaster.getResponseId()).get();
                if (Objects.nonNull(response)) {
                    response = ResponseMaster.builder()
                            .itemId(itemMaster)
                            .response(responseMaster.getResponse())
                            .rating(responseMaster.getRating())
                            .createdDate(responseMaster.getCreatedDate())
                            .updatedDate(responseMaster.getUpdatedDate())
                            .createdBy(responseMaster.getCreatedBy())
                            .updatedBy(responseMaster.getUpdatedBy())
                            .status(responseMaster.getStatus())
                            .build();
                    responseMasterRepository.save(response);
                }

            }

            return itemMaster;

        } else {
            throw new Exception("Invalid Input " + itemMasterUpdatePojo);
        }

    }
}
