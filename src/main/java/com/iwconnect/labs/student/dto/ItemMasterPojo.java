package com.iwconnect.labs.student.dto;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.iwconnect.labs.student.domain.ResponseMaster;
import lombok.Data;

import java.util.List;

@Data
public class ItemMasterPojo {

    private Long itemId;
    private Long slNo;

    private String item;

    private List<ResponseMaster> response;

    private String category;

    private String focusArea;

    private String createdDate;

    private String updatedDate;

    private String createdBy;

    private String updatedBy;

    private String status;

}
