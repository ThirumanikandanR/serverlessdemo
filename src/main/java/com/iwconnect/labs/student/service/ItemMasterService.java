package com.iwconnect.labs.student.service;

import com.iwconnect.labs.student.domain.ItemMaster;
import com.iwconnect.labs.student.dto.ItemMasterPojo;
import com.iwconnect.labs.student.dto.ItemMasterUpdatePojo;
import org.springframework.stereotype.Service;

@Service
public interface ItemMasterService {
    ItemMaster saveItemDetails(ItemMasterPojo itemMasterPojo) throws Exception;

    ItemMaster getItemById(String itemId);

    ItemMaster updateItemDetails(ItemMasterUpdatePojo itemMasterUpdatePojo) throws Exception;
}
