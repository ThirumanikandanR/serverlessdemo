package com.iwconnect.labs.student.service.impl;

import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.iwconnect.labs.student.domain.ResponseMaster;
import com.iwconnect.labs.student.repository.ResponseMasterRepository;
import com.iwconnect.labs.student.service.ResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class ResponseServiceImpl implements ResponseService {

    @Autowired
    ResponseMasterRepository responseMasterRepository;
    @Override
    public ResponseMaster getResponseById(String responseId) {
      Optional<ResponseMaster> response= responseMasterRepository.findById(responseId);
        if (response.isPresent() && Objects.nonNull(response)) {
            return response.get();
        } else {
            throw new ResourceNotFoundException("Response not found with id: " + responseId);
        }
    }
}
