package com.iwconnect.labs.student.repository;

import com.iwconnect.labs.student.domain.ItemMaster;
import com.iwconnect.labs.student.domain.ResponseMaster;
import com.iwconnect.labs.student.domain.Student;
import org.springframework.data.repository.CrudRepository;

public interface ResponseMasterRepository extends CrudRepository<ResponseMaster, String> {
}
