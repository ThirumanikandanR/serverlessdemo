package com.iwconnect.labs.conf;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.iwconnect.labs.student.domain.ItemMaster;
import com.iwconnect.labs.student.domain.ResponseMaster;
import com.iwconnect.labs.student.service.ResponseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Function;

@Configuration
public class ResponseFunction {

    private static Logger logger = LoggerFactory.getLogger(FunctionConfiguration.class);

    @Autowired
    ResponseService responseService;

    @Bean
    public Function<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> findResponseById() {
        logger.info("Execute findItemById method");

        return (proxyRequestEvent) -> {
            APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
            try {
                String responseId = proxyRequestEvent.getQueryStringParameters().get("responseId");

                System.out.println("responseId:" + responseId);
                logger.info("\"responseId:\" + responseId");
                ResponseMaster response = responseService.getResponseById(responseId);
                responseEvent.setStatusCode(200);
                responseEvent.setBody(response.toString());
                return responseEvent;
            } catch (Exception e) {
                e.printStackTrace();
                return new APIGatewayProxyResponseEvent().withStatusCode(500);
            }
        };
    }


}
