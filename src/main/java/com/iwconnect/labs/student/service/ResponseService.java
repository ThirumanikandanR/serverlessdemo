package com.iwconnect.labs.student.service;

import com.iwconnect.labs.student.domain.ResponseMaster;
import org.springframework.stereotype.Service;

@Service
public interface ResponseService {
    ResponseMaster getResponseById(String responseId);

}
